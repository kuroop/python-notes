# ============
# python-notes
# ============

a lot of snippets, to explore python

### 532 out of 100000 entries done

[01-oct-2018 - 52 entries](./notes-01-oct-2018.md)

[03-oct-2018 - 27 entries](./notes-03-oct-2018.md)

[12-oct-2018 - 14 entries](./notes-12-oct-2018.md)

[14-oct-2018 - 27 entries](./notes-14-oct-2018.md)

[15-oct-2018 - 22 entries](./notes-15-oct-2018.md)

[16-oct-2018 - 05 entries](./notes-16-oct-2018.md)

[17-oct-2018 - 22 entries](./notes-17-oct-2018.md)

[18-oct-2018 - 20 entries](./notes-18-oct-2018.md)

[19-oct-2018 - 30 entries](./notes-19-oct-2018.md)

[20-oct-2018 - 03 entries](./notes-20-oct-2018.md)

[21-oct-2018 - 15 entries](./notes-21-oct-2018.md)

[22-oct-2018 - 12 entries](./notes-22-oct-2018.md)

[23-oct-2018 - 11 entries](./notes-23-oct-2018.md)

[24-oct-2018 - 02 entries](./notes-24-oct-2018.md)

[25-oct-2018 - 04 entries](./notes-25-oct-2018.md)

[26-oct-2018 - 08 entries](./notes-26-oct-2018.md)

[27-oct-2018 - 07 entries](./notes-27-oct-2018.md)

[28-oct-2018 - 16 entries](./notes-28-oct-2018.md)

[29-oct-2018 - 03 entries](./notes-29-oct-2018.md)

[30-oct-2018 - 10 entries](./notes-30-oct-2018.md)

[31-oct-2018 - 07 entries](./notes-31-oct-2018.md)

[01-nov-2018 - 07 entries](./notes-01-nov-2018.md)

[02-nov-2018 - 05 entries](./notes-02-nov-2018.md)

[03-nov-2018 - 06 entries](./notes-03-nov-2018.md)

[04-nov-2018 - 15 entries](./notes-04-nov-2018.md)

[05-nov-2018 - 11 entries](./notes-05-nov-2018.md)

[06-nov-2018 - 08 entries](./notes-06-nov-2018.md)

[07-nov-2018 - 06 entries](./notes-07-nov-2018.md)

[08-nov-2018 - 15 entries](./notes-08-nov-2018.md)

[09-nov-2018 - 06 entries](./notes-09-nov-2018.md)

[10-nov-2018 - 12 entries](./notes-10-nov-2018.md)

[11-nov-2018 - 12 entries](./notes-11-nov-2018.md)

[12-nov-2018 - 10 entries](./notes-12-nov-2018.md)

[13-nov-2018 - 04 entries](./notes-13-nov-2018.md)

[14-nov-2018 - 16 entries](./notes-14-nov-2018.md)

[15-nov-2018 - 18 entries](./notes-15-nov-2018.md)

[16-nov-2018 - 03 entries](./notes-16-nov-2018.md)

[17-nov-2018 - 02 entries](./notes-17-nov-2018.md)

[18-nov-2018 - 05 entries](./notes-18-nov-2018.md)

[19-nov-2018 - 01 entries](./notes-19-nov-2018.md)

[20-nov-2018 - 04 entries](./notes-20-nov-2018.md)

[21-nov-2018 - 05 entries](./notes-21-nov-2018.md)

[22-nov-2018 - 03 entries](./notes-22-nov-2018.md)

[23-nov-2018 - 06 entries](./notes-23-nov-2018.md)

[24-nov-2018 - 10 entries](./notes-24-nov-2018.md)

[25-nov-2018 - 01 entries](./notes-25-nov-2018.md)

[26-nov-2018 - 01 entries](./notes-26-nov-2018.md)

[27-nov-2018 - 10 entries](./notes-27-nov-2018.md)

[01-dec-2018 - 05 entries](./notes-01-dec-2018.md)

[02-dec-2018 - 05 entries](./notes-02-dec-2018.md)

[03-dec-2018 - 03 entries](./notes-03-dec-2018.md)




