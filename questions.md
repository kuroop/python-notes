# Questions in my mind

- Why interpreter can't import python3. What stops us to design the whole thing in a way that Interpreter is decoupled from the version of python.
- What benefit we get from not using flex/bison set of tools and doing some stuff manually?
- What files I can remove from Cpython and still get a subset of python which is having same expressive power ?
- Why can't I install libraries dynamically while a python program is running?
- Why can't programming languages we merged with each other, for example kind of a git merge and we can use each other them inside each other. Is it possible design languages like that?
- Can I do elementary cellular automata visualization as a network of nodes ? What kind of graphs it will create ? Can binary string be considered as a graph representation ? if so how ?
- Is there a browser based pylinter kind of plugin which will detect python scripts and tell which version of python is this script for?
- Why is that stack is used in compilers and not queues? Why do we use stacks in programming languages? May be it's cuz we want recent things to be avaialble faster and it helps in organizing the computation better - ~~~not sure~~~ Any kind of heirarchical structure traversing will be most efficient with stack maybe ?
- Does try and finally combination achieve anything? It does not catch any exceptions and finally anyways get executed afterwards. So why have it at all ? : may be it just makes sure that finally runs, even if there is a exception in the program(check)
- Can we combine fuse and StringIO/BytesIO to make a inmemory filesystem. Can we use that inmemory filesystem for installing programs in memory.[in-memory-fs](https://askubuntu.com/questions/152868/how-do-i-make-a-ram-disk)
